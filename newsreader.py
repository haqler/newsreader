# Sat Mar 16 10:12:46 EDT 2019

import sys
import re

def usage():
    print "Usage: newsreader.py input_file_name"
    return 0


if len(sys.argv) < 2:
    usage()
    quit(1)


input_file = sys.argv[1]
with open(input_file, 'r') as f:
    file_content = f.read()
    delim_re = re.compile(' +[1-9]+ of [1-9]+ DOCUMENTS')
    articles = delim_re.split(file_content)

    # Remove first element which contains only whitespaces and line breaks
    del articles[0]

    for article_data in articles:
        article_rows = article_data.splitlines()
        newspaper_name = article_rows[4].strip()
        article_time = article_rows[6].strip()
        article_title = article_rows[10].strip()
        article_text = '\n'.join(article_rows[19:-14])
        
        print "Newspaper name: " + newspaper_name
        print "Article date: " + article_time
        print "Article title: " + article_title
        print "\n" + article_text
        print '========================================'
